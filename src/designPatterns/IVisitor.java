package designPatterns;

import ast.interfaces.IIntegerNumber;
import ast.interfaces.IOperation;

public interface IVisitor {

	void visitBinaryOperation(IOperation operation);
	void visitIntegerNumber(IIntegerNumber integerNumber);
	void visitUnaryOperation(IOperation operation);

}
