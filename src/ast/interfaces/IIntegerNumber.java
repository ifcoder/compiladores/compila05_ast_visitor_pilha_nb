package ast.interfaces;

public interface IIntegerNumber extends IExpression {

	int getValue();

}
