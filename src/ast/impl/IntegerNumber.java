package ast.impl;

import designPatterns.IVisitor;
import ast.interfaces.*;

public class IntegerNumber
        implements IIntegerNumber {

    private int value;

    public IntegerNumber(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void accept(IVisitor visitor) {
        visitor.visitIntegerNumber(this);
    }

}

