package ast.impl;

import ast.interfaces.*;

public class Factory implements designPatterns.IFactory {

    public Factory() {
    }

    public IIntegerNumber createIntegerNumber(int i) {
        return new IntegerNumber(i);
    }

    public IUnaryOperation createUnaryOperation(int code, IExpression operand) {
        return new UnaryOperation(code, operand);
    }

    public ast.interfaces.IBinaryOperation createBinaryOperation(int code, IExpression operand1, IExpression operand2) {
        return new BinaryOperation(code, operand1, operand2);
    }

}
